# giom-infra-commons

Common place for utilities

Installation
- tested on python 3.10.16
- if using html2pdf, requires wkhtmltopdf system install (can be wkhtmltox on some)
- require cairo and such if using invoice pdf converted annotation

Todo - CI/CD
- [X] create gitlab CI build with proper docker image and install
- [X] eventually create DockerFile
- [ ] switch to PyMuPDF 
  - [X] for better PDF format testing
  - [ ] for PDF generation apart from weasyprint?
- [ ] add coverage
- [ ] split in 2 packages to avoid stacking unnecessary system dependencies on lib clients using only some subparts
