from io import BytesIO
from zipfile import ZipFile

import pyzipper as pyzipper


def list_protected_zipfile(source, password) -> [str]:
    with ZipFile(source) as zip_file:
        zip_file.setpassword(bytes(password, "utf-8"))
        namelist = zip_file.namelist()
        pdf_file_name = namelist[0]


def extract_first_pdf_from_zip_bytes(source: bytes, password) -> bytes:
    with BytesIO(source) as io:
        return extract_first_pdf_from_zipfile(
            io, password
        ).read()


def extract_first_pdf_from_zipfile(file:BytesIO, password):
    with ZipFile(file, mode='r') as zip_file:
        zip_file.setpassword(bytes(password, "utf-8"))
        namelist = zip_file.namelist()
        pdf_file_name = namelist[0]
        print("file contains %s" % pdf_file_name)
        with zip_file.open(pdf_file_name) as pdf_file:
            return pdf_file


def decrypt_first_file_from_aes_zipfile(zipfile, password):
    with pyzipper.AESZipFile(BytesIO(zipfile.read()), mode='r') as zip_file:
        zip_file.setpassword(bytes(password, "UTF-8")) #@todo: check if encoding is needed or if I take it straight up
        namelist = zip_file.namelist()
        encrypted_filename = namelist[0]
        decrypted_content = zip_file.read(encrypted_filename)
        return encrypted_filename, decrypted_content

def decrypt_file_from_aes_zipfile(zipfile, password, encrypted_filename):
    with pyzipper.AESZipFile(BytesIO(zipfile.read()), mode='r') as zip_file:
        zip_file.setpassword(bytes(password, "UTF-8")) #@todo: check if encoding is needed or if I take it straight up
        decrypted_content = zip_file.read(encrypted_filename)
        return decrypted_content

def list_files_in_aes_zipfile(zipfile, password):
    with pyzipper.AESZipFile(BytesIO(zipfile.read()), mode='r') as zip_file:
        zip_file.setpassword(bytes(password, "UTF-8"))  # @todo: check if encoding is needed or if I take it straight up
        return zip_file.namelist()

