import os
import random
import tempfile
import logging
# logging.getLogger('weasyprint').setLevel(logging.DEBUG)

from flask import request, url_for

from giom.flaskutils.to_pdf_wrapper import pdf_convertible, is_font_present_in_pdf, is_pdf_valid, extract_pdf_fonts
from tests.flaskutils.pdf_tests.flask_test_utils import FlaskContextTestUtils


class TestHtml2PdfEndpointWrapper(FlaskContextTestUtils):

    def test_returns_pdf_when_decorator_set(self, app):

        @app.route('/as_pdf')
        @pdf_convertible(stylesheets=[])
        def pdfiseable():
            return "coucou"

        def not_pdf():
            return '<html><div class="company-title">Un titre</div></html>'

        app.add_url_rule('/as_pdf', view_func=pdfiseable)
        app.add_url_rule('/not_pdf', view_func=not_pdf)

        with app.test_client() as c:
            r_pdf = c.get('/as_pdf', headers={'format': 'pdf'})
            r_not_pdf = c.get('/not_pdf', headers={'format': 'pdf'})

        assert 'application/pdf' != r_not_pdf.mimetype, "not pdf mimetype when decorator not set"
        assert 'application/pdf' == r_pdf.mimetype, "pdf mimetype when decorator set"

        result = is_pdf_valid(r_pdf.data), "result if a pdf when decorator set"
        assert result[0], f'result is a pdf while decorator set ({result[1]})'
        result = is_pdf_valid(r_not_pdf.data)
        assert not result[0], f'result is not a pdf while decorator not set: ({result[1]})'

    def test_returns_pdf_and_pass_along_args(self, app):

        @app.route('/as_pdf')
        @pdf_convertible(stylesheets=[])
        def pdf_route():
            return "coucou %s" % request.json.get('name')

        app.add_url_rule('/as_pdf', view_func=pdf_route)

        with app.test_client() as c:
            r_pdf = c.get('/as_pdf', headers={'format': 'pdf'}, json={'name': 'titi'})

        assert 200 == r_pdf.status_code
        assert 'application/pdf' == r_pdf.mimetype

    def test_decorator_should_apply_css_as_set(self, app):

        with app.app_context():
            css = url_for('static', filename='test_css.css')

            @pdf_convertible(stylesheets=[css])
            def pdfiseable_css():
                return '<html><body><div class="company-title">Un titre</div><body></html>'

            @pdf_convertible(stylesheets=[])
            def pdfiseable_no_css():
                return '<html><body><div class="company-title">Un titre</div><body></html>'

            app.add_url_rule('/pdf_css', view_func=pdfiseable_css)
            app.add_url_rule('/pdf_without_css', view_func=pdfiseable_no_css)

        with app.test_client() as c:
            r_with_css = c.get('/pdf_css', headers={'format': 'pdf'})
            r_without_css = c.get('/pdf_without_css', headers={'format': 'pdf'})
            # store r_with_css.data in a file to check it is a pdf
            debug_filename = os.path.join(tempfile.gettempdir(), f'debug_generated_pdf_{random.randint(0, 999)}.pdf')
            f = open(debug_filename, 'wb')
            f.write(r_without_css.data)
            f.close()
            print(f'pdf saved at {debug_filename}')

            try:
                assert is_font_present_in_pdf(r_with_css.data, 'Arial'), "css applied"
                assert not is_font_present_in_pdf(r_without_css.data, 'Arial'), "css not applied"
            except AssertionError as e:
                print(f'font when applied: {extract_pdf_fonts(r_with_css.data)}')
                print(f'font when not applied: {extract_pdf_fonts(r_with_css.data)}')
                raise

    def test_decorator_should_not_do_pdf_without_content_type_header(self, app):

        @pdf_convertible(stylesheets=[])
        def pdfiseable():
            return '<html><div class="company-title">Un titre</div></html>'

        app.add_url_rule('/pdf', view_func=pdfiseable)

        with app.test_client() as c:
            r_pdf = c.get('/pdf', headers={'format': 'pdf'})
            r_not_pdf = c.get('pdf')
            assert 'application/pdf' != r_not_pdf.mimetype, "pdf while no header"
            assert 'application/pdf' == r_pdf.mimetype, "not pdf while header is set"
